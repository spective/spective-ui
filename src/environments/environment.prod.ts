export const environment = {
  production: true,
  apiHost: "api.spective.alexk8s.com",
  httpProtocol: "https://",
  websocketProtocol: "wss://"
};
