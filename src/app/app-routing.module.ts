import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '@auth0/auth0-angular';

const routes: Routes = [{
  path: 'admin',
  loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule)
}, {
  path: 'search',
  loadChildren: () => import('./search/search.module').then(m => m.SearchModule),
  canActivate: [AuthGuard]
},
  {
    path: 'manage-following',
    loadChildren: () => import('./manage-following/manage-following.module').then(m => m.ManageFollowingModule),
    canActivate: [AuthGuard]
  },
  {
    path:"",
    pathMatch:"full",
    redirectTo:"search"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
