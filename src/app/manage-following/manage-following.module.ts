import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManageFollowingRoutingModule } from './manage-following-routing.module';
import { ManageFollowingComponent } from './manage-following.component';
import {NzListModule} from 'ng-zorro-antd/list';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzTypographyModule} from 'ng-zorro-antd/typography';


@NgModule({
  declarations: [
    ManageFollowingComponent
  ],
  imports: [
    CommonModule,
    ManageFollowingRoutingModule,
    NzListModule,
    NzIconModule,
    NzButtonModule,
    NzTypographyModule
  ]
})
export class ManageFollowingModule { }
