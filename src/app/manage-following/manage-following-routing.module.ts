import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ManageFollowingComponent } from './manage-following.component';

const routes: Routes = [{ path: '', component: ManageFollowingComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageFollowingRoutingModule { }
