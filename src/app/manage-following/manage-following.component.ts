import { Component, OnInit } from '@angular/core';
import {Following, FollowService} from '../service/follow.service';
import {Observable, ReplaySubject} from 'rxjs';
import {delay, tap} from 'rxjs/operators';

@Component({
  selector: 'app-manage-following',
  templateUrl: './manage-following.component.html',
  styleUrls: ['./manage-following.component.scss']
})
export class ManageFollowingComponent implements OnInit {

  constructor(private followService:FollowService) { }

  following$?:Observable<Array<Following>>;
  followingCount$=new ReplaySubject(1);
  ngOnInit(): void {
    this.following$ = this.followService.getFollowing()
      .pipe(delay(100),tap(f=>this.followingCount$.next(f.length)))
    ;
  }

  unfollow(author:string){
    this.followService.unfollow(author).subscribe();
  }

}
