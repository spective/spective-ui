import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {environment} from '../environments/environment';
import {NzLayoutModule} from 'ng-zorro-antd/layout';
import {NzMenuModule} from 'ng-zorro-antd/menu';
import {IconDefinition} from '@ant-design/icons-angular';
import {
  CheckCircleTwoTone,
  CloseCircleTwoTone,
  EyeFill,
  HeartFill,
  HeartOutline,
  BugOutline
} from '@ant-design/icons-angular/icons';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {TimeagoModule} from 'ngx-timeago';
import {AuthHttpInterceptor, AuthModule} from '@auth0/auth0-angular';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';


const icons: IconDefinition[] = [CheckCircleTwoTone, CloseCircleTwoTone, EyeFill, HeartOutline, HeartFill,BugOutline];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NzLayoutModule,
    NzMenuModule,
    NzIconModule.forRoot(icons),
    TimeagoModule.forRoot(),
    HttpClientModule,
    AuthModule.forRoot({
      domain: 'dev-oj0ok9xc.us.auth0.com',
      clientId: 'A3TJ3fU28lkAqcWVl00vYBSB85RGTgvl',
      audience: 'https://api.spective.io',
      httpInterceptor: {
        allowedList: [{
          uri: `${environment.httpProtocol}${environment.apiHost}/*`,
          tokenOptions: {audience: "https://api.spective.io"}
        },]
      },
    })
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: AuthHttpInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
