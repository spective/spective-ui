import {HttpClient} from "@angular/common/http";
import {Component, OnDestroy, OnInit} from "@angular/core";
import {FormControl, FormGroup} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {interval, NEVER, Observable, of, Subscription} from "rxjs";
import {concatMap, delay, delayWhen, filter, map, shareReplay, switchMap} from "rxjs/operators";
import {environment} from "../../environments/environment";
import {RSocketService} from "../service/r-socket.service";
import {PostStore} from "./post.store";

@Component({
  selector: "app-search",
  templateUrl: "./search.component.html",
  styleUrls: ["./search.component.scss"]
})
export class SearchComponent implements OnInit, OnDestroy {

  searchControl = new FormGroup({
    subreddit: new FormControl(),
    author: new FormControl(),
    title: new FormControl(),
    filterUnsafe: new FormControl()
  });

  subredditOptions$?: Observable<Array<string>>;
  authorOptions$?: Observable<Array<string>>;
  postStore: PostStore = new PostStore();
  pauseLoad: Observable<any> = NEVER;
  loadingSubscription?: Subscription;
  postsLoading = false;
  error?: any;


  constructor(private http: HttpClient, private rsocketService: RSocketService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.subredditOptions$ = this.searchControl.get("subreddit")?.valueChanges
      .pipe(
        switchMap((x) => this.getTypeaheadOptions(x, "SUBREDDIT", this.searchControl.get("filterUnsafe")?.value || false))
      );
    this.authorOptions$ = this.searchControl.get("author")?.valueChanges
      .pipe(
        switchMap((x) => this.getTypeaheadOptions(x, "AUTHOR", this.searchControl.get("filterUnsafe")?.value || false))
      );
    this.route.queryParams.subscribe(params => {
      this.loadData(params as SearchCriteria);
      this.searchControl.reset(params);
    });
    this.pauseLoad = interval(1000).pipe(
      map((x) => SearchComponent.getScrollPercentage()),
      map((percent) => percent < 70 && this.postStore.getPosts().length > 10),
      shareReplay(1),
      filter(v => !v)
    );

  }

  private getTypeaheadOptions(query: string, type: string, safeOnly: boolean): Observable<Array<string>> {
    if (!query || query.length < 3) {
      return of([]);
    }
    return this.http.get<Array<string>>(`${environment.httpProtocol}${environment.apiHost}/v1/search/typeahead/${type}`, {
      params: {
        query,
        safeOnly
      }
    });
  }

  public search() {
    this.router.navigate(["./"], {relativeTo: this.route, queryParams: this.searchControl.value});
  }

  loadData(searchCriteria?: SearchCriteria) {
    if (searchCriteria && (searchCriteria.author || searchCriteria.subreddit)) {
      this.postsLoading = true;
      this.loadingSubscription?.unsubscribe();
      this.postStore = new PostStore();
      this.loadingSubscription = this.rsocketService.requestStream("search", searchCriteria)
        .pipe(
          concatMap(x => of(x).pipe(delay(100), delayWhen(() => this.pauseLoad))),
        )
        .subscribe({
          next: x => {
            this.postStore.add(x);
          },
          error: e => this.error = e.message,
          complete: () => this.postsLoading = false
        });
    }

  }

  private static getScrollPercentage(): number {
    const height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
    if (height <= 100)//if the document is too small or if the scroll height is being overridden return zero so all posts aren't loaded
      return 0;
    return ((document.documentElement.scrollTop + document.body.scrollTop) / height * 100);
  }

  ngOnDestroy(): void {
    this.loadingSubscription?.unsubscribe();
  }


}

interface SearchCriteria {
  subreddit: String;
  author: String;
  title: String;
}
