export interface Post {
  title: string;
  author: string;
  subreddit: string;
  originalUrl: string;
  createdTime: string;
  type: string;
  viewed?: boolean;
  url: string;
  id: string;
}

export interface ContentMetadata {
  postedUrl: string;
  originalUrl: string;
  type: string;
  origin: string;
  originId: string;
  contentType: string;
  hash: string;
  imageHash: string;
  size: number;
}

export interface RedditPost {
  id: string;
  author: string;
  title: string;
  url: string;
  subreddit: string;
  source: string;
  createdTime: string;
  over18: boolean;
  scrapeTime: string;
  scrapeHost: string;
  ingestTime: string;
  ingestHost: string;
  gallery: boolean;
}

export enum Duplicate {
  NONE = "NONE",
  URL = "URL",
  HASH = "HASH",
  IMAGE_HASH = "IMAGE_HASH"
}

export interface ResolvedPost {
  post: Post;
  redditPost: RedditPost;
  contentMetadata: ContentMetadata;
  duplicate: Duplicate;
  duplicateCounts: Map<Duplicate, number>
}
