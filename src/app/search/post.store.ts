import {Duplicate, ResolvedPost} from "./post";


export class PostStore {
  private posts: Array<ResolvedPost> = [];
  private postsByHash = new Map<string, ResolvedPost>();
  private postsByImageHash = new Map<string, ResolvedPost>();
  private postsByUrl = new Map<string, ResolvedPost>();
  private queuedDuplicates = new Set<ResolvedPost>();

  public add(post: ResolvedPost) {
    if (post.duplicate === Duplicate.NONE) {
      this.posts.push(post);
      this.postsByUrl.set(post.redditPost.url, post);
      this.postsByHash.set(post.contentMetadata.hash, post);
      if (post.contentMetadata.imageHash) {
        this.postsByImageHash.set(post.contentMetadata.imageHash, post);
      }
      post.duplicateCounts = new Map();
      const toRemove = [...this.queuedDuplicates.values()].filter((p) => this.incrementDuplicateCount(p));
      toRemove.forEach((p) => this.queuedDuplicates.delete(p));
    } else {
      if (!this.incrementDuplicateCount(post))
        this.queuedDuplicates.add(post);
    }
  }

  public getPosts(): Readonly<Array<ResolvedPost>> {
    return this.posts;
  }

  private incrementDuplicateCount(post: ResolvedPost): boolean {
    let originalPost: ResolvedPost | undefined;
    switch (post.duplicate) {
      case Duplicate.HASH:
        originalPost = this.postsByHash.get(post.contentMetadata.hash);
        break;
      case Duplicate.IMAGE_HASH:
        originalPost = this.postsByImageHash.get(post.contentMetadata.imageHash);
        break;
      case Duplicate.URL:
        originalPost = this.postsByUrl.get(post.redditPost.url);
        break;
    }
    if (!originalPost) {
      return false;
    } else {
      const count = originalPost.duplicateCounts.get(post.duplicate) || 0;
      originalPost.duplicateCounts.set(post.duplicate, count + 1);
      return true;
    }
  }
}
