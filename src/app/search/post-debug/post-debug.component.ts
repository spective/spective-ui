import {Component, Input} from "@angular/core";
import {ViewService} from "../../service/view.service";
import {ResolvedPost} from "../post";

@Component({
  selector: "app-post-debug",
  templateUrl: "./post-debug.component.html",
  styleUrls: ["./post-debug.component.scss"]
})
export class PostDebugComponent {
  @Input()
  post!: ResolvedPost;
  isVisible = false;

  constructor(private viewService: ViewService) {
  }

  showModal(): void {
    this.isVisible = true;
    this.viewService.addOverlay();
  }

  handleOk(): void {
    this.isVisible = false;
  }

  handleCancel(): void {
    this.isVisible = false;
  }
}
