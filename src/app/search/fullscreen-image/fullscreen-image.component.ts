import {Component, EventEmitter, HostListener, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-fullscreen-image',
  templateUrl: './fullscreen-image.component.html',
  styleUrls: ['./fullscreen-image.component.scss']
})
export class FullscreenImageComponent {

  @Input()
  url!:string;
  @Output()
  close = new EventEmitter<MouseEvent>();
  constructor() { }

  @HostListener("click",['$event'])
  emitClose(event:MouseEvent){
    this.close.emit(event);
  }
}
