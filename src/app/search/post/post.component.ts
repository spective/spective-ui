import {Component, Input, OnInit} from '@angular/core';
import { ResolvedPost} from '../post';
import {ActivatedRoute, Router} from '@angular/router';
import {RSocketService} from '../../service/r-socket.service';
import {ViewService} from '../../service/view.service';
import {FollowService} from '../../service/follow.service';
import {Observable} from 'rxjs';
import {concatMap, map, shareReplay, take} from 'rxjs/operators';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

  @Input()
  post!: ResolvedPost;
  fullscreen = false;
  showAuthorDrawer = false;
  fullscreenStartTime = 0;
  videoPlayStartTime = 0;
  isFollowing$?: Observable<boolean>;

  constructor(private router: Router, private route: ActivatedRoute, private rsocket: RSocketService, private viewService: ViewService,
              private followingService: FollowService) {
  }

  navigateSubreddit() {
    this.router.navigate(['./'], {relativeTo: this.route, queryParams: {subreddit: this.post.post.subreddit}});
  }

  ngOnInit(): void {
    this.rsocket.requestResponse("view/get", this.post.post).subscribe(view => this.post.post.viewed = !!view);
    this.isFollowing$ = this.followingService.getFollowing()
      .pipe(map(following => following.findIndex(f => f.author === this.post.post.author) !== -1), shareReplay(1));
  }

  toggleFullscreen(): void {
    if (this.fullscreen) {
      const duration = new Date().getTime() - this.fullscreenStartTime;
      this.fullscreen = false;
      this.fullscreenStartTime = 0;
      this.viewService.saveView(this.post.post, duration, true);
    } else {
      this.viewService.addOverlay();
      this.fullscreen = true;
      this.fullscreenStartTime = new Date().getTime();
      if (this.videoPlayStartTime) {
        this.stopVideoPlay();
      }
    }
  }

  openAuthorDrawer(): void {
    this.showAuthorDrawer = true;
    this.viewService.addOverlay();
  }

  startVideoPlay() {
    this.videoPlayStartTime = new Date().getTime();
  }

  stopVideoPlay() {
    if (this.videoPlayStartTime) {
      const duration = new Date().getTime() - this.videoPlayStartTime;
      this.videoPlayStartTime = 0;
      this.viewService.saveView(this.post.post, duration, false);
    }
  }

  toggleFollow() {
    const author = this.post.post.author;
    this.isFollowing$?.pipe(
      take(1),
      concatMap(f => f ? this.followingService.unfollow(author) : this.followingService.follow(author))
    ).subscribe();
  }
}
