import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {ReactiveFormsModule} from "@angular/forms";
import {ExportOutline} from "@ant-design/icons-angular/icons";
import {NzAlertModule} from "ng-zorro-antd/alert";
import {NzAutocompleteModule} from "ng-zorro-antd/auto-complete";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzCardModule} from "ng-zorro-antd/card";
import {NzWaveModule} from "ng-zorro-antd/core/wave";
import {NzDrawerModule} from "ng-zorro-antd/drawer";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzInputModule} from "ng-zorro-antd/input";
import {NzLayoutModule} from "ng-zorro-antd/layout";
import {NzModalModule} from "ng-zorro-antd/modal";
import {NzSwitchModule} from "ng-zorro-antd/switch";
import {NzTagModule} from "ng-zorro-antd/tag";
import {NzTypographyModule} from "ng-zorro-antd/typography";
import {TimeagoModule} from "ngx-timeago";
import {AuthorPostsComponent} from "./author-posts/author-posts.component";
import {FullscreenImageComponent} from "./fullscreen-image/fullscreen-image.component";
import {InViewDirective} from "./in-view.directive";
import {PostDebugComponent} from "./post-debug/post-debug.component";
import {PostComponent} from "./post/post.component";

import {SearchRoutingModule} from "./search-routing.module";
import {SearchComponent} from "./search.component";


@NgModule({
  declarations: [
    SearchComponent,
    PostComponent,
    AuthorPostsComponent,
    FullscreenImageComponent,
    InViewDirective,
    PostDebugComponent
  ],
  imports: [
    CommonModule,
    SearchRoutingModule,
    NzLayoutModule,
    NzFormModule,
    ReactiveFormsModule,
    NzAutocompleteModule,
    NzInputModule,
    NzTypographyModule,
    NzWaveModule,
    NzButtonModule,
    NzCardModule,
    NzDrawerModule,
    TimeagoModule.forChild(),
    NzIconModule,
    NzAlertModule,
    NzIconModule.forChild([ExportOutline]),
    NzSwitchModule,
    NzTagModule,
    NzModalModule
  ]
})
export class SearchModule {
}
