import {Component, Input, OnDestroy, OnInit} from "@angular/core";
import {Subscription} from "rxjs";
import {RSocketService} from "../../service/r-socket.service";
import {ViewService} from "../../service/view.service";
import {ResolvedPost} from "../post";
import {PostStore} from "../post.store";

@Component({
  selector: "app-author-posts",
  templateUrl: "./author-posts.component.html",
  styleUrls: ["./author-posts.component.scss"]
})
export class AuthorPostsComponent implements OnInit, OnDestroy {

  @Input()
  author!: String;
  posts = new PostStore();
  fullscreenPost?: ResolvedPost;
  postSubscription?: Subscription;
  fullscreenStartTime = 0;
  videoPlayStartTime = 0;
  playingVideo?: ResolvedPost;

  constructor(private rsocket: RSocketService, private viewService: ViewService) {
  }

  ngOnInit(): void {
    this.postSubscription = this.rsocket.requestStream("search", {author: this.author})
      .subscribe((x) => this.posts.add(x));
  }

  ngOnDestroy(): void {
    this.postSubscription?.unsubscribe();
  }

  toggleFullscreen(post?: ResolvedPost) {
    if (!this.fullscreenPost) {
      this.viewService.addOverlay();
      this.fullscreenPost = post;
      this.fullscreenStartTime = new Date().getTime();
      this.stopVideoPlay();
    } else {
      const duration = new Date().getTime() - this.fullscreenStartTime;
      this.fullscreenStartTime = 0;
      this.viewService.saveView(this.fullscreenPost!.post, duration, true);
      this.fullscreenPost = undefined;
      if (this.playingVideo) {
        this.startVideoPlay(this.playingVideo);
      }
    }
  }

  startVideoPlay(post: ResolvedPost) {
    this.videoPlayStartTime = new Date().getTime();
    this.playingVideo = post;
  }

  stopVideoPlay() {
    if (this.videoPlayStartTime) {
      const duration = new Date().getTime() - this.videoPlayStartTime;
      this.videoPlayStartTime = 0;
      this.viewService.saveView(this.playingVideo!.post, duration, false);
    }
  }

}
