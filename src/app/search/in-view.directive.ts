import {AfterViewInit, Directive, HostListener, Input, OnDestroy, OnInit, ViewContainerRef} from '@angular/core';
import {Post, ResolvedPost} from './post';
import {ViewService} from '../service/view.service';
import {Subscription} from 'rxjs';
import {delay} from 'rxjs/operators';

@Directive({
  selector: '[appInView]'
})
export class InViewDirective implements AfterViewInit, OnInit, OnDestroy {

  constructor(private vcRef: ViewContainerRef, private viewService: ViewService) {
  }

  @Input()
  post?: ResolvedPost;
  private inViewport: boolean = false;
  private viewStartTime: number = 0;
  private wasUnfocused = false;
  private drawerSubscription?: Subscription;

  ngAfterViewInit(): void {
    if (!this.post) {
      return;
    }
    const element = this.vcRef.element.nativeElement;
    const observer = new IntersectionObserver(entries => {
      entries.forEach(entry => {
        this.inViewport = entry.intersectionRatio >= .7;
        this.updateView();
      });
    }, {threshold: [0, .7]});
    observer.observe(element);
  }

  updateView() {
    if (this.inViewport && this.viewStartTime === 0) { //view path1
      this.viewStartTime = new Date().getTime();
    } else if (this.viewStartTime !== 0) { //view path 2
      const duration = new Date().getTime() - this.viewStartTime;
      this.viewStartTime = 0;
      this.viewService.saveView(this.post!.post, duration, false);
    }
  }



  //to handle window changes
  @HostListener("window:blur")
  public handleBlur() {
    if (!this.wasUnfocused) {
      this.updateView();
    }
    this.wasUnfocused = true;
  }

  @HostListener("window:focus")
  public handleFocus() {
    if (this.wasUnfocused) {
      this.viewStartTime = 0;
      this.updateView();
    }
    this.wasUnfocused = false;
  }

  ngOnDestroy(): void {
    this.inViewport = false;
    this.updateView();
    this.drawerSubscription?.unsubscribe();
  }

  ngOnInit(): void {
    this.drawerSubscription = this.viewService.overlayAdded()
      .pipe(delay(50))//delay to allow elements to be reordered
      .subscribe(() => {
      this.inViewport = false;
      this.updateView();
    });
  }


}
