import {Injectable} from '@angular/core';
import {RSocketService} from './r-socket.service';
import {Post} from '../search/post';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ViewService {

  private overlayAddedSubject = new Subject();

  constructor(private rsocket: RSocketService) {
  }

  saveView(post: Post, duration: number, fullscreen: boolean) {
    this.rsocket.fireAndForget("view", {post, duration, fullscreen});
  }

  addOverlay() {
    this.overlayAddedSubject.next(1);
  }

  overlayAdded(): Observable<any> {
    return this.overlayAddedSubject;
  }
}
