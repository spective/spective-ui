import {Injectable, OnInit} from '@angular/core';
import {
  BufferEncoders,
  encodeBearerAuthMetadata,
  encodeCompositeMetadata,
  encodeRoute,
  MESSAGE_RSOCKET_AUTHENTICATION,
  MESSAGE_RSOCKET_COMPOSITE_METADATA,
  MESSAGE_RSOCKET_ROUTING,
  RSocketClient
} from 'rsocket-core';
import RSocketWebSocketClient from 'rsocket-websocket-client';
import {environment} from '../../environments/environment';
import {AuthService} from '@auth0/auth0-angular';
import {combineLatest, defer, iif, Observable, of, ReplaySubject, Subject, throwError} from 'rxjs';
import {concatMap, distinctUntilChanged, map, shareReplay, switchMap, take, takeUntil, tap} from 'rxjs/operators';
import {ConnectionStatus, ReactiveSocket} from 'rsocket-types/ReactiveSocketTypes';

@Injectable({
  providedIn: 'root'
})
export class RSocketService implements OnInit {

  private client?: Observable<RSocketClient<any, any>>;
  private rsocket?: Observable<ReactiveSocket<any, any>>;

  constructor(private authService: AuthService) {
    this.client = this.authService.getAccessTokenSilently()
      .pipe(map(RSocketService.buildClient), shareReplay(1));

    this.rsocket = this.client.pipe(
      switchMap(client=>RSocketService.connectClient(client)),
      shareReplay(1)
    );
  }

  ngOnInit(): void {

  }


  private static buildClient(token: string): RSocketClient<any, any> {
    return new RSocketClient<any, any>({
      setup: {
        keepAlive: 20000,
        lifetime: 200000,
        dataMimeType: 'application/json',
        metadataMimeType: MESSAGE_RSOCKET_COMPOSITE_METADATA.string,
        payload: {
          metadata: encodeCompositeMetadata([
            [MESSAGE_RSOCKET_AUTHENTICATION, encodeBearerAuthMetadata(token)]
          ])
        }
      },
      transport: new RSocketWebSocketClient({
        url: `${environment.websocketProtocol}${environment.apiHost}/rsocket`
      },BufferEncoders)
    });
  }

  private static connectClient(client: RSocketClient<any, any>): Observable<ReactiveSocket<any, any>> {
    const socketSubject = new ReplaySubject<ReactiveSocket<any, any>>(1);
    const socketStatusSubject = new ReplaySubject<ConnectionStatus>(1);
    client.connect().then(socket => {
      socketSubject.next(socket);
      socket.connectionStatus().subscribe(status=>{
        socketStatusSubject.next(status);
      });
    });
    return combineLatest([socketSubject, socketStatusSubject])
      .pipe(
        concatMap(([socket, status]) => iif(() =>status.kind === 'CONNECTED' || status.kind === "CONNECTING", of(socket), defer(() => this.connectClient(client)))),
        distinctUntilChanged()
      );
  }

  requestStream(route: string, data?: any): Observable<any> {
    const disposed=new Subject();
    return this.rsocket?.pipe(concatMap(socket => {
      const responseSubject = new Subject<any>();
      try{

      socket.requestStream({
        data: Buffer.from(JSON.stringify(data)),
        metadata: encodeCompositeMetadata([[MESSAGE_RSOCKET_ROUTING, encodeRoute(route)]])
      }).subscribe({
        onNext: payload => {
          responseSubject.next(JSON.parse(payload.data.toString()));
        },
        onError:e=>{
          responseSubject.error(e)
        },
        onComplete:()=>{
          responseSubject.complete();
          disposed.next(1);
      },
        onSubscribe:(subscription)=>{
         subscription.request(10000);
      }

      });}
      catch(e){
        responseSubject.error(e)
      }
      return responseSubject.asObservable();
    }),takeUntil(disposed)) || throwError("no socket");
  }

  requestResponse(route:string,data?:any):Observable<any>{

    return this.rsocket?.pipe(concatMap(socket => {
      const responseSubject = new Subject<any>();
      try{

        socket.requestResponse({
          data: Buffer.from(JSON.stringify(data)),
          metadata: encodeCompositeMetadata([[MESSAGE_RSOCKET_ROUTING, encodeRoute(route)]])
        }).subscribe({
          onError:e=>{
            responseSubject.error(e)
          },
          onComplete:(payload)=>{
            responseSubject.next(JSON.parse(payload.data.toString()));
            responseSubject.complete();
          },
        });}
      catch(e){
        responseSubject.error(e)
      }
      return responseSubject;
    })) || throwError("no socket");
  }

  fireAndForget(route:string,data?:any){

    this.rsocket?.pipe(take(1)).subscribe(socket => {
      try{

        socket.fireAndForget({
          data: Buffer.from(JSON.stringify(data)),
          metadata: encodeCompositeMetadata([[MESSAGE_RSOCKET_ROUTING, encodeRoute(route)]])
        })}
      catch(e){
        console.error(e);
      }
    }) || throwError("no socket");
  }
}
