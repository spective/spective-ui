import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, ReplaySubject, Subject} from 'rxjs';
import {environment} from '../../environments/environment';
import {publishLast, shareReplay, switchMap, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FollowService {

  private followingUpdateSubject = new ReplaySubject();
  private readonly following$: Observable<Array<Following>>;

  constructor(private http: HttpClient) {
    this.following$ = this.followingUpdateSubject.pipe(
      switchMap(() => this.http.get<Array<Following>>(`${environment.httpProtocol}${environment.apiHost}/v1/following`)),
      shareReplay(1),
    );
    this.followingUpdateSubject.next(1);
  }

  getFollowing(): Observable<Array<Following>> {
    return this.following$;
  }

  follow(author: string): Observable<Following> {
    return this.http.post<Following>(`${environment.httpProtocol}${environment.apiHost}/v1/follow`, author)
      .pipe(tap(() => this.followingUpdateSubject.next(1)));
  }

  unfollow(author: string): Observable<any> {
    return this.http.post<never>(`${environment.httpProtocol}${environment.apiHost}/v1/unfollow`, author)
      .pipe(tap(() => this.followingUpdateSubject.next(1)));
  }

}

export interface Following {
  user: string;
  author: string;
  authorDeleted: boolean;
  followDate: string;
}

