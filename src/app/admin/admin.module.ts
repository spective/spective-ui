import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import {NzLayoutModule} from 'ng-zorro-antd/layout';
import {NzMenuModule} from 'ng-zorro-antd/menu';
import { IndexMigrationComponent } from './index-migration/index-migration.component';
import {HttpClientModule} from '@angular/common/http';
import {NzListModule} from 'ng-zorro-antd/list';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzWaveModule} from 'ng-zorro-antd/core/wave';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzProgressModule} from 'ng-zorro-antd/progress';
import {NzTagModule} from 'ng-zorro-antd/tag';
import {NzGridModule} from 'ng-zorro-antd/grid';
import { CountPipe } from './index-migration/count.pipe';


@NgModule({
  declarations: [
    AdminComponent,
    IndexMigrationComponent,
    CountPipe,
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    NzLayoutModule,
    NzMenuModule,
    HttpClientModule,
    NzListModule,
    NzIconModule,
    NzWaveModule,
    NzButtonModule,
    NzProgressModule,
    NzTagModule,
    NzGridModule
  ]
})
export class AdminModule { }
