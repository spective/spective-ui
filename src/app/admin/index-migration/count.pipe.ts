import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'count'
})
export class CountPipe implements PipeTransform {

  transform(value: number, ...args: unknown[]): string {
    if(!value)
      return "0";
    if(value<9000){
      return value+"";
    }
    if(value<900000){
      return (value/1000).toFixed(2)+"k";
    }
    if(value<900000000){
      return (value/1000000).toFixed(2)+"m";
    }
    return (value/1000000000).toFixed(2)+"b";
  }

}
