import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-index-migration',
  templateUrl: './index-migration.component.html',
  styleUrls: ['./index-migration.component.scss']
})
export class IndexMigrationComponent implements OnInit {

  indexes$: Observable<Array<Index>>;

  constructor(private http: HttpClient) {
    this.indexes$ = http.get<Array<Index>>(`${environment.httpProtocol}${environment.apiHost}/v1/migration/elasticsearch/indexes`);
  }

  ngOnInit(): void {
  }

  migrate(index: Index): void {
    index.showProgress = true;
    index.migrating=true;
    index.migrationPercent = 0;
    index.migrationEstimation = "TBD";
    // this.rsocketService.route("migrate-index").data(index).answerMimetype("application/json").requestStream().subscribe({
    //   next: (x: IndexMigration) => {
    //     index.migrationPercent = x.record_count / index.documentCount * 100;
    //     index.migratedCount=x.record_count;
    //     const duration = new Date(x.end_time).getTime() - new Date(x.start_time).getTime();
    //     const secondsRemaining = (100 - index.migrationPercent) / index.migrationPercent * duration / 1000;
    //     index.migrationEstimation = `${Math.floor(secondsRemaining / 3600)}h ${Math.floor(secondsRemaining / 60 % 60)}m ${Math.floor(secondsRemaining % 60)}s`;
    //     index.migrationPercent = Math.round(index.migrationPercent);
    //   }, error: (err) => {index.error = err;
    //     debugger;},
    //   complete: () => {
    //     index.migrating=false;
    //     index.migrated = index.migrationPercent == 100;
    //   }
    // });
  }

}

interface Index {
  name: string;
  migrated: boolean;
  size: string;
  documentCount: number;
  showProgress: boolean;
  migrating:boolean;
  migrationPercent: number;
  migrationEstimation: string;
  migratedCount: number;
  error: string;
  hasDumpFile:boolean;
  alreadyMigratedCount:number;
}

interface IndexMigration {
  version: string;
  index: string;
  start_time: string;
  end_time: string;
  record_count: number;
}
