import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AdminComponent} from './admin.component';
import {IndexMigrationComponent} from './index-migration/index-migration.component';

const routes: Routes = [{
  path: '', component: AdminComponent,
  children: [{path: 'index-migration', component: IndexMigrationComponent}]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {
}
